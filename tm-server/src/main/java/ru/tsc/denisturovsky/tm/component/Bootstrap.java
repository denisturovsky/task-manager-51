package ru.tsc.denisturovsky.tm.component;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.tsc.denisturovsky.tm.api.endpoint.*;
import ru.tsc.denisturovsky.tm.api.service.*;
import ru.tsc.denisturovsky.tm.api.service.dto.*;
import ru.tsc.denisturovsky.tm.dto.model.UserDTO;
import ru.tsc.denisturovsky.tm.endpoint.*;
import ru.tsc.denisturovsky.tm.enumerated.Role;
import ru.tsc.denisturovsky.tm.sevice.AuthService;
import ru.tsc.denisturovsky.tm.sevice.ConnectionService;
import ru.tsc.denisturovsky.tm.sevice.LoggerService;
import ru.tsc.denisturovsky.tm.sevice.PropertyService;
import ru.tsc.denisturovsky.tm.sevice.dto.*;
import ru.tsc.denisturovsky.tm.util.SystemUtil;

import javax.xml.ws.Endpoint;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

public final class Bootstrap implements IServiceLocator {

    @NotNull
    private final IAuthEndpoint authEndpoint = new AuthEndpoint(this);

    @NotNull
    private final IProjectEndpoint projectEndpoint = new ProjectEndpoint(this);

    @Getter
    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @Getter
    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @Getter
    @NotNull
    private final ILoggerService loggerService = new LoggerService(connectionService);

    @Getter
    @NotNull
    private final IProjectDTOService projectService = new ProjectDTOService(connectionService);

    @Getter
    @NotNull
    private final ITaskDTOService taskService = new TaskDTOService(connectionService);

    @Getter
    @NotNull
    private final IProjectTaskDTOService projectTaskService = new ProjectTaskDTOService(projectService, taskService);

    @Getter
    @NotNull
    private final IUserDTOService userService = new UserDTOService(propertyService, connectionService, projectService, taskService);

    @Getter
    @NotNull
    private final ISessionDTOService sessionService = new SessionDTOService(connectionService);

    @Getter
    @NotNull
    private final IAuthService authService = new AuthService(propertyService, userService, sessionService);

    @NotNull
    private final ISystemEndpoint systemEndpoint = new SystemEndpoint(this);

    @NotNull
    private final ITaskEndpoint taskEndpoint = new TaskEndpoint(this);

    @NotNull
    private final IUserEndpoint userEndpoint = new UserEndpoint(this);

    {
        registry(systemEndpoint);
        registry(projectEndpoint);
        registry(taskEndpoint);
        registry(userEndpoint);
        registry(authEndpoint);
    }

    private void initDemoData() {
        try {
            @NotNull final UserDTO admin = userService.create("admin", "admin", Role.ADMIN);
        } catch (@NotNull final Exception e) {
            System.out.println(e.getMessage());
        }
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String fileName = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(fileName), pid.getBytes());
        @NotNull final File file = new File(fileName);
        file.deleteOnExit();
    }

    private void prepareShutdown() {
        loggerService.info("** TASK-MANAGER SERVER STOPPED **");
    }

    private void registry(@NotNull final Object endpoint) {
        @NotNull final String host = propertyService.getServerHost();
        @NotNull final String port = propertyService.getServerPort();
        @NotNull final String name = endpoint.getClass().getSimpleName();
        @NotNull final String url = "http://" + host + ":" + port + "/" + name + "?wsdl";
        Endpoint.publish(url, endpoint);
        System.out.println(url);
    }

    public void run() {
        initPID();
        initDemoData();
        loggerService.initJmsLogger();
        loggerService.info("** TASK-MANAGER SERVER STARTED **");
        Runtime.getRuntime().addShutdownHook(new Thread(this::prepareShutdown));
    }

}