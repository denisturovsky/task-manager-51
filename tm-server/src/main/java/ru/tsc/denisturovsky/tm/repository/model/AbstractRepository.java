package ru.tsc.denisturovsky.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.denisturovsky.tm.api.FieldConstants;
import ru.tsc.denisturovsky.tm.api.repository.model.IRepository;
import ru.tsc.denisturovsky.tm.comparator.CreatedComparator;
import ru.tsc.denisturovsky.tm.comparator.StatusComparator;
import ru.tsc.denisturovsky.tm.model.AbstractModel;
import ru.volnenko.lib.util.GenericUtil;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractRepository<M extends AbstractModel> implements IRepository<M> {

    @NotNull
    protected final EntityManager entityManager;

    public AbstractRepository(@NotNull final EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @NotNull
    @Override
    public M add(@NotNull final M model) throws Exception {
        entityManager.persist(model);
        return model;
    }

    @Override
    public void clear() throws Exception {
        @Nullable final List<M> models = findAll();
        if (models == null) return;
        for (@NotNull final M model : models) {
            remove(model);
        }
    }

    @Override
    public boolean existsById(@NotNull final String id) throws Exception {
        return findOneById(id) != null;
    }

    @Nullable
    @Override
    public List<M> findAll() throws Exception {
        @NotNull final String jpql = "SELECT m FROM " + getEntityClass().getSimpleName() + " m";
        return entityManager.createQuery(jpql, getEntityClass()).getResultList();
    }

    @Nullable
    @Override
    public List<M> findAll(@Nullable final Comparator comparator) throws Exception {
        @NotNull final CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        @NotNull final CriteriaQuery<M> criteriaQuery = criteriaBuilder.createQuery(getEntityClass());
        @NotNull final Root<M> from = criteriaQuery.from(getEntityClass());
        criteriaQuery.select(from);
        criteriaQuery.orderBy(criteriaBuilder.asc(from.get(getSortType(comparator))));
        return entityManager.createQuery(criteriaQuery).getResultList();
    }

    @Nullable
    @Override
    public M findOneById(@NotNull final String id) throws Exception {
        return entityManager.find(getEntityClass(), id);
    }

    @SuppressWarnings("unchecked")
    protected Class<M> getEntityClass() {
        return (Class<M>) GenericUtil.getClassFirst(this.getClass());
    }

    @Override
    public int getSize() throws Exception {
        @NotNull final String jpql = "SELECT COUNT(m) FROM " + getEntityClass().getSimpleName() + " m";
        return entityManager.createQuery(jpql, Long.class).getSingleResult().intValue();
    }

    @NotNull
    protected String getSortType(@NotNull final Comparator comparator) {
        if (comparator == CreatedComparator.INSTANCE) return FieldConstants.COLUMN_CREATED;
        if (comparator == StatusComparator.INSTANCE) return FieldConstants.COLUMN_STATUS;
        else return FieldConstants.COLUMN_NAME;
    }

    @Override
    public void remove(@NotNull final M model) throws Exception {
        entityManager.remove(model);
    }

    @Override
    public void update(@NotNull final M model) throws Exception {
        entityManager.merge(model);
    }

}
