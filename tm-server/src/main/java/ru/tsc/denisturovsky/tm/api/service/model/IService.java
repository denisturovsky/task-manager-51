package ru.tsc.denisturovsky.tm.api.service.model;

import org.jetbrains.annotations.Nullable;
import ru.tsc.denisturovsky.tm.api.repository.model.IRepository;
import ru.tsc.denisturovsky.tm.enumerated.Sort;
import ru.tsc.denisturovsky.tm.model.AbstractModel;

import java.util.List;

public interface IService<M extends AbstractModel> extends IRepository<M> {

    @Nullable
    List<M> findAll(@Nullable Sort sort) throws Exception;

    void removeOneById(@Nullable String id) throws Exception;

}
