package ru.tsc.denisturovsky.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.tsc.denisturovsky.tm.api.service.IConnectionService;
import ru.tsc.denisturovsky.tm.api.service.IPropertyService;
import ru.tsc.denisturovsky.tm.api.service.dto.IProjectDTOService;
import ru.tsc.denisturovsky.tm.api.service.dto.ITaskDTOService;
import ru.tsc.denisturovsky.tm.api.service.dto.IUserDTOService;
import ru.tsc.denisturovsky.tm.comparator.NameComparator;
import ru.tsc.denisturovsky.tm.dto.model.ProjectDTO;
import ru.tsc.denisturovsky.tm.dto.model.UserDTO;
import ru.tsc.denisturovsky.tm.enumerated.Sort;
import ru.tsc.denisturovsky.tm.enumerated.Status;
import ru.tsc.denisturovsky.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.denisturovsky.tm.exception.field.*;
import ru.tsc.denisturovsky.tm.marker.UnitCategory;
import ru.tsc.denisturovsky.tm.sevice.ConnectionService;
import ru.tsc.denisturovsky.tm.sevice.PropertyService;
import ru.tsc.denisturovsky.tm.sevice.dto.ProjectDTOService;
import ru.tsc.denisturovsky.tm.sevice.dto.TaskDTOService;
import ru.tsc.denisturovsky.tm.sevice.dto.UserDTOService;

import javax.persistence.EntityNotFoundException;
import java.util.Comparator;
import java.util.List;

import static ru.tsc.denisturovsky.tm.constant.ProjectTestData.*;
import static ru.tsc.denisturovsky.tm.constant.UserTestData.USER_TEST_LOGIN;
import static ru.tsc.denisturovsky.tm.constant.UserTestData.USER_TEST_PASSWORD;

@Category(UnitCategory.class)
public final class ProjectServiceTest {

    @NotNull
    private static final IPropertyService PROPERTY_SERVICE = new PropertyService();

    @NotNull
    private static final IConnectionService CONNECTION_SERVICE = new ConnectionService(PROPERTY_SERVICE);

    @NotNull
    private static final IProjectDTOService SERVICE = new ProjectDTOService(CONNECTION_SERVICE);

    @NotNull
    private static final ITaskDTOService TASK_SERVICE = new TaskDTOService(CONNECTION_SERVICE);

    @NotNull
    private static final IUserDTOService USER_SERVICE = new UserDTOService(PROPERTY_SERVICE, CONNECTION_SERVICE, SERVICE, TASK_SERVICE);

    @NotNull
    private static String USER_ID = "";

    @BeforeClass
    public static void setUp() throws Exception {
        @NotNull final UserDTO user = USER_SERVICE.create(USER_TEST_LOGIN, USER_TEST_PASSWORD);
        USER_ID = user.getId();
    }

    @AfterClass
    public static void tearDown() throws Exception {
        @Nullable final UserDTO user = USER_SERVICE.findByLogin(USER_TEST_LOGIN);
        if (user != null) USER_SERVICE.remove(user);
        CONNECTION_SERVICE.close();
    }

    @Test
    public void addByUserId() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> SERVICE.add(null, USER_PROJECT3));
        Assert.assertNotNull(SERVICE.add(USER_ID, USER_PROJECT3));
        @Nullable final ProjectDTO project = SERVICE.findOneById(USER_ID, USER_PROJECT3.getId());
        Assert.assertNotNull(project);
        Assert.assertEquals(USER_PROJECT3.getId(), project.getId());
    }

    @After
    public void after() throws Exception {
        SERVICE.clear(USER_ID);
    }

    @Before
    public void before() throws Exception {
        SERVICE.add(USER_ID, USER_PROJECT1);
        SERVICE.add(USER_ID, USER_PROJECT2);
    }

    @Test
    public void changeProjectStatusById() throws Exception {
        @NotNull final Status status = Status.COMPLETED;
        Assert.assertThrows(UserIdEmptyException.class, () -> SERVICE.changeProjectStatusById(null, USER_PROJECT1.getId(), status));
        Assert.assertThrows(UserIdEmptyException.class, () -> SERVICE.changeProjectStatusById("", USER_PROJECT1.getId(), status));
        Assert.assertThrows(IdEmptyException.class, () -> SERVICE.changeProjectStatusById(USER_ID, null, status));
        Assert.assertThrows(IdEmptyException.class, () -> SERVICE.changeProjectStatusById(USER_ID, "", status));
        Assert.assertThrows(StatusEmptyException.class, () -> SERVICE.changeProjectStatusById(USER_ID, USER_PROJECT1.getId(), null));
        Assert.assertThrows(ProjectNotFoundException.class, () -> SERVICE.changeProjectStatusById(USER_ID, NON_EXISTING_PROJECT_ID, status));
        SERVICE.changeProjectStatusById(USER_ID, USER_PROJECT1.getId(), status);
        @Nullable final ProjectDTO project = SERVICE.findOneById(USER_ID, USER_PROJECT1.getId());
        Assert.assertNotNull(project);
        Assert.assertEquals(status, project.getStatus());
    }

    @Test
    public void clearByUserId() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> SERVICE.clear(""));
        SERVICE.clear(USER_ID);
        Assert.assertEquals(0, SERVICE.getSize(USER_ID));
    }

    @Test
    public void create() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> SERVICE.create(null, USER_PROJECT3.getName()));
        Assert.assertThrows(UserIdEmptyException.class, () -> SERVICE.create("", USER_PROJECT3.getName()));
        Assert.assertThrows(NameEmptyException.class, () -> SERVICE.create(USER_ID, null));
        Assert.assertThrows(NameEmptyException.class, () -> SERVICE.create(USER_ID, ""));
        @NotNull final ProjectDTO project = SERVICE.create(USER_ID, USER_PROJECT3.getName());
        Assert.assertNotNull(project);
        @Nullable final ProjectDTO findProject = SERVICE.findOneById(USER_ID, project.getId());
        Assert.assertNotNull(findProject);
        Assert.assertEquals(project.getId(), findProject.getId());
        Assert.assertEquals(USER_PROJECT3.getName(), project.getName());
        Assert.assertEquals(USER_ID, project.getUserId());
    }

    @Test
    public void createWithDescription() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> SERVICE.create(null, USER_PROJECT3.getName(), USER_PROJECT3.getDescription()));
        Assert.assertThrows(UserIdEmptyException.class, () -> SERVICE.create("", USER_PROJECT3.getName(), USER_PROJECT3.getDescription()));
        Assert.assertThrows(NameEmptyException.class, () -> SERVICE.create(USER_ID, null, USER_PROJECT3.getDescription()));
        Assert.assertThrows(NameEmptyException.class, () -> SERVICE.create(USER_ID, "", USER_PROJECT3.getDescription()));
        Assert.assertThrows(DescriptionEmptyException.class, () -> SERVICE.create(USER_ID, USER_PROJECT3.getName(), null));
        Assert.assertThrows(DescriptionEmptyException.class, () -> SERVICE.create(USER_ID, USER_PROJECT3.getName(), ""));
        @NotNull final ProjectDTO project = SERVICE.create(USER_ID, USER_PROJECT3.getName(), USER_PROJECT3.getDescription());
        Assert.assertNotNull(project);
        @Nullable final ProjectDTO findProject = SERVICE.findOneById(USER_ID, project.getId());
        Assert.assertNotNull(findProject);
        Assert.assertEquals(project.getId(), findProject.getId());
        Assert.assertEquals(USER_PROJECT3.getName(), project.getName());
        Assert.assertEquals(USER_PROJECT3.getDescription(), project.getDescription());
        Assert.assertEquals(USER_ID, project.getUserId());
    }

    @Test
    public void existsByIdByUserId() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> SERVICE.existsById(null, NON_EXISTING_PROJECT_ID));
        Assert.assertThrows(UserIdEmptyException.class, () -> SERVICE.existsById("", NON_EXISTING_PROJECT_ID));
        Assert.assertFalse(SERVICE.existsById(USER_ID, ""));
        Assert.assertFalse(SERVICE.existsById(USER_ID, NON_EXISTING_PROJECT_ID));
        Assert.assertTrue(SERVICE.existsById(USER_ID, USER_PROJECT1.getId()));
    }

    @Test
    public void findAllByUserId() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> SERVICE.findAll(""));
        final List<ProjectDTO> projects = SERVICE.findAll(USER_ID);
        Assert.assertNotNull(projects);
        Assert.assertEquals(2, projects.size());
        projects.forEach(project -> Assert.assertEquals(USER_ID, project.getUserId()));
    }

    @Test
    public void findAllComparatorByUserId() throws Exception {
        @Nullable Comparator comparator = null;
        Assert.assertNotNull(SERVICE.findAll(USER_ID, comparator));
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            @Nullable Comparator comparatorInner = null;
            SERVICE.findAll("", comparatorInner);
        });
        comparator = NameComparator.INSTANCE;
        final List<ProjectDTO> projects = SERVICE.findAll(USER_ID, comparator);
        Assert.assertNotNull(projects);
        projects.forEach(project -> Assert.assertEquals(USER_ID, project.getUserId()));
    }

    @Test
    public void findAllSortByUserId() throws Exception {
        @Nullable Sort sort = null;
        Assert.assertNotNull(SERVICE.findAll(USER_ID, sort));
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            @Nullable Sort sortInner = null;
            SERVICE.findAll("", sortInner);
        });
        sort = Sort.BY_NAME;
        final List<ProjectDTO> projects = SERVICE.findAll(USER_ID, sort);
        Assert.assertNotNull(projects);
        projects.forEach(project -> Assert.assertEquals(USER_ID, project.getUserId()));
    }

    @Test
    public void findOneByIdByUserId() throws Exception {
        Assert.assertThrows(IdEmptyException.class, () -> SERVICE.findOneById(USER_ID, ""));
        Assert.assertThrows(UserIdEmptyException.class, () -> SERVICE.existsById("", USER_PROJECT1.getId()));
        Assert.assertNull(SERVICE.findOneById(USER_ID, NON_EXISTING_PROJECT_ID));
        @Nullable final ProjectDTO project = SERVICE.findOneById(USER_ID, USER_PROJECT1.getId());
        Assert.assertNotNull(project);
        Assert.assertEquals(USER_PROJECT1.getId(), project.getId());
    }

    @Test
    public void getSizeByUserId() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> SERVICE.getSize(""));
        Assert.assertEquals(2, SERVICE.getSize(USER_ID));
    }

    @Test
    public void removeByIdByUserId() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> SERVICE.removeOneById(null, null));
        Assert.assertThrows(UserIdEmptyException.class, () -> SERVICE.removeOneById("", null));
        Assert.assertThrows(IdEmptyException.class, () -> SERVICE.removeOneById(USER_ID, null));
        Assert.assertThrows(IdEmptyException.class, () -> SERVICE.removeOneById(USER_ID, ""));
        Assert.assertThrows(EntityNotFoundException.class, () -> SERVICE.removeOneById(USER_ID, NON_EXISTING_PROJECT_ID));
        SERVICE.removeOneById(USER_ID, USER_PROJECT2.getId());
        Assert.assertNull(SERVICE.findOneById(USER_ID, USER_PROJECT2.getId()));
    }

    @Test
    public void removeByUserId() throws Exception {
        SERVICE.remove(USER_ID, USER_PROJECT2);
        Assert.assertNull(SERVICE.findOneById(USER_ID, USER_PROJECT2.getId()));
    }

    @Test
    public void updateById() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> SERVICE.updateOneById(null, USER_PROJECT1.getId(), USER_PROJECT1.getName(), USER_PROJECT1.getDescription()));
        Assert.assertThrows(UserIdEmptyException.class, () -> SERVICE.updateOneById("", USER_PROJECT1.getId(), USER_PROJECT1.getName(), USER_PROJECT1.getDescription()));
        Assert.assertThrows(IdEmptyException.class, () -> SERVICE.updateOneById(USER_ID, null, USER_PROJECT1.getName(), USER_PROJECT1.getDescription()));
        Assert.assertThrows(IdEmptyException.class, () -> SERVICE.updateOneById(USER_ID, "", USER_PROJECT1.getName(), USER_PROJECT1.getDescription()));
        Assert.assertThrows(NameEmptyException.class, () -> SERVICE.updateOneById(USER_ID, USER_PROJECT1.getId(), null, USER_PROJECT1.getDescription()));
        Assert.assertThrows(NameEmptyException.class, () -> SERVICE.updateOneById(USER_ID, USER_PROJECT1.getId(), "", USER_PROJECT1.getDescription()));
        Assert.assertThrows(DescriptionEmptyException.class, () -> SERVICE.updateOneById(USER_ID, USER_PROJECT1.getId(), USER_PROJECT1.getName(), null));
        Assert.assertThrows(DescriptionEmptyException.class, () -> SERVICE.updateOneById(USER_ID, USER_PROJECT1.getId(), USER_PROJECT1.getName(), ""));
        Assert.assertThrows(ProjectNotFoundException.class, () -> SERVICE.updateOneById(USER_ID, NON_EXISTING_PROJECT_ID, USER_PROJECT1.getName(), USER_PROJECT1.getDescription()));
        @NotNull final String name = USER_PROJECT1.getName() + NON_EXISTING_PROJECT_ID;
        @NotNull final String description = USER_PROJECT1.getDescription() + NON_EXISTING_PROJECT_ID;
        SERVICE.updateOneById(USER_ID, USER_PROJECT1.getId(), name, description);
        @Nullable final ProjectDTO project = SERVICE.findOneById(USER_ID, USER_PROJECT1.getId());
        Assert.assertNotNull(project);
        Assert.assertEquals(name, project.getName());
        Assert.assertEquals(description, project.getDescription());
    }

}