<?xml version="1.0" encoding="UTF-8"?>
<project xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns="http://maven.apache.org/POM/4.0.0"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <groupId>ru.tsc.denisturovsky.tm</groupId>
    <artifactId>task-manager-server</artifactId>
    <version>1.0.0</version>

    <name>Task Manager Server</name>
    <url>https://gitlab.com/denisturovsky</url>

    <properties>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <maven.compiler.source>1.8</maven.compiler.source>
        <maven.compiler.target>1.8</maven.compiler.target>
    </properties>

    <profiles>
        <profile>
            <id>Data</id>
            <activation>
                <activeByDefault>true</activeByDefault>
            </activation>
            <properties>
                <test.category>ru.tsc.denisturovsky.tm.marker.DataCategory</test.category>
            </properties>
        </profile>
        <profile>
            <id>UNIT</id>
            <activation>
                <activeByDefault>false</activeByDefault>
            </activation>
            <properties>
                <test.category>ru.tsc.denisturovsky.tm.marker.UnitCategory</test.category>
            </properties>
        </profile>
    </profiles>

    <scm>
        <connection>scm:svn:http://none</connection>
        <developerConnection>scm:svn:http://none</developerConnection>
        <url>scm:svn:http://none</url>
    </scm>

    <developers>
        <developer>
            <id>dturovsky</id>
            <name>Denis Turovsky</name>
            <email>shveikko@gmail.com</email>
            <organization>TSC</organization>
        </developer>
    </developers>

    <repositories>
        <repository>
            <id>public.artifactory.volnenko.ru</id>
            <name>artifactory.volnenko.ru</name>
            <url>http://artifactory.volnenko.ru/artifactory/list/public/</url>
        </repository>
    </repositories>

    <dependencies>
        <dependency>
            <groupId>ru.tsc.denisturovsky.tm</groupId>
            <artifactId>task-manager-domain</artifactId>
            <version>1.0.0</version>
        </dependency>
        <dependency>
            <groupId>ru.volnenko.lib</groupId>
            <artifactId>basic-util</artifactId>
            <version>1.0.0</version>
        </dependency>
        <dependency>
            <groupId>org.apache.activemq</groupId>
            <artifactId>activemq-all</artifactId>
            <version>5.15.9</version>
        </dependency>
        <dependency>
            <groupId>org.hsqldb</groupId>
            <artifactId>hsqldb</artifactId>
            <version>2.4.1</version>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.hibernate</groupId>
            <artifactId>hibernate-c3p0</artifactId>
            <version>5.6.1.Final</version>
        </dependency>
        <dependency>
            <groupId>com.hazelcast</groupId>
            <artifactId>hazelcast</artifactId>
            <version>3.12</version>
        </dependency>
        <dependency>
            <groupId>com.hazelcast</groupId>
            <artifactId>hazelcast-hibernate53</artifactId>
            <version>1.3.2</version>
        </dependency>
        <dependency>
            <groupId>org.postgresql</groupId>
            <artifactId>postgresql</artifactId>
            <version>42.3.1</version>
        </dependency>
        <dependency>
            <groupId>mysql</groupId>
            <artifactId>mysql-connector-java</artifactId>
            <version>8.0.30</version>
        </dependency>
        <dependency>
            <groupId>com.jcabi</groupId>
            <artifactId>jcabi-manifests</artifactId>
            <version>1.1</version>
        </dependency>
        <dependency>
            <groupId>org.slf4j</groupId>
            <artifactId>slf4j-api</artifactId>
            <version>1.7.32</version>
        </dependency>
        <dependency>
            <groupId>org.slf4j</groupId>
            <artifactId>slf4j-log4j12</artifactId>
            <version>1.7.32</version>
        </dependency>
        <dependency>
            <groupId>org.jetbrains</groupId>
            <artifactId>annotations</artifactId>
            <version>22.0.0</version>
        </dependency>
        <dependency>
            <groupId>org.projectlombok</groupId>
            <artifactId>lombok</artifactId>
            <version>1.18.10</version>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>org.reflections</groupId>
            <artifactId>reflections</artifactId>
            <version>0.9.11</version>
        </dependency>
        <dependency>
            <groupId>junit</groupId>
            <artifactId>junit</artifactId>
            <version>4.13</version>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>com.fasterxml.jackson.core</groupId>
            <artifactId>jackson-databind</artifactId>
            <version>2.12.5</version>
        </dependency>
        <dependency>
            <groupId>com.fasterxml.jackson.dataformat</groupId>
            <artifactId>jackson-dataformat-yaml</artifactId>
            <version>2.12.5</version>
        </dependency>
        <dependency>
            <groupId>com.fasterxml.jackson.dataformat</groupId>
            <artifactId>jackson-dataformat-xml</artifactId>
            <version>2.12.5</version>
        </dependency>
        <dependency>
            <groupId>org.eclipse.persistence</groupId>
            <artifactId>org.eclipse.persistence.moxy</artifactId>
            <version>2.7.2</version>
        </dependency>
        <dependency>
            <groupId>javax.xml.bind</groupId>
            <artifactId>jaxb-api</artifactId>
            <version>2.3.1</version>
        </dependency>
    </dependencies>

    <build>
        <finalName>task-manager-server</finalName>
        <plugins>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-jar-plugin</artifactId>
                <version>3.2.1</version>
                <configuration>
                    <archive>
                        <manifest>
                            <addClasspath>true</addClasspath>
                            <mainClass>ru.tsc.denisturovsky.tm.Application</mainClass>
                        </manifest>
                        <manifestEntries>
                            <buildNumber>${buildNumber}</buildNumber>
                            <version>${project.version}</version>
                            <timestamp>${timestamp}</timestamp>
                            <artifactId>${project.artifactId}</artifactId>
                            <groupId>${project.groupId}</groupId>
                            <developer>Denis Turovsky</developer>
                            <email>dturovsky@t1-consulting.ru</email>
                        </manifestEntries>
                    </archive>
                </configuration>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-shade-plugin</artifactId>
                <version>3.3.0</version>
                <executions>
                    <execution>
                        <phase>package</phase>
                        <goals>
                            <goal>shade</goal>
                        </goals>
                        <configuration>
                            <transformers>
                                <transformer
                                        implementation="org.apache.maven.plugins.shade.resource.ManifestResourceTransformer">
                                    <mainClass>ru.tsc.denisturovsky.tm.Application</mainClass>
                                </transformer>
                                <transformer
                                        implementation="org.apache.maven.plugins.shade.resource.AppendingTransformer">
                                    <resource>META-INF/services/com.hazelcast.DataSerializerHook</resource>
                                </transformer>
                            </transformers>
                            <filters>
                                <filter>
                                    <artifact>*:*</artifact>
                                    <excludes>
                                        <exclude>META-INF/*.SF</exclude>
                                        <exclude>META-INF/*.DSA</exclude>
                                        <exclude>META-INF/*.RSA</exclude>
                                    </excludes>
                                </filter>
                            </filters>
                        </configuration>
                    </execution>
                </executions>
            </plugin>
            <plugin>
                <groupId>org.codehaus.mojo</groupId>
                <artifactId>buildnumber-maven-plugin</artifactId>
                <version>1.4</version>
                <executions>
                    <execution>
                        <phase>generate-resources</phase>
                        <goals>
                            <goal>create</goal>
                        </goals>
                    </execution>
                </executions>
                <configuration>
                    <doCheck>true</doCheck>
                    <doUpdate>false</doUpdate>
                    <revisionOnScmFailure>true</revisionOnScmFailure>
                    <format>${project.version}.{0,number}</format>
                    <items>
                        <item>buildNumber</item>
                        <item>timestamp</item>
                    </items>
                </configuration>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-surefire-plugin</artifactId>
                <version>3.0.0-M5</version>
                <configuration>
                    <groups>${test.category}</groups>
                </configuration>
            </plugin>
            <!--            <plugin>-->
            <!--                <groupId>org.liquibase</groupId>-->
            <!--                <artifactId>liquibase-maven-plugin</artifactId>-->
            <!--                <version>3.6.3</version>-->
            <!--                <configuration>-->
            <!--                    <outputChangeLogFile>${project.basedir}/src/main/resources/changelog/changelog-master.xml-->
            <!--                    </outputChangeLogFile>-->
            <!--                    <changeLogFile>${project.basedir}/src/main/resources/changelog/changelog-master.xml</changeLogFile>-->
            <!--                    <url>jdbc:postgresql://localhost:5432/postgres</url>-->
            <!--                    <username>postgres</username>-->
            <!--                    <password>postgres</password>-->
            <!--                </configuration>-->
            <!--                <dependencies>-->
            <!--                    <dependency>-->
            <!--                        <groupId>org.postgresql</groupId>-->
            <!--                        <artifactId>postgresql</artifactId>-->
            <!--                        <version>42.3.1</version>-->
            <!--                    </dependency>-->
            <!--                </dependencies>-->
            <!--            </plugin>-->
        </plugins>
    </build>

</project>