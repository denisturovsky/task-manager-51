package ru.tsc.denisturovsky.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.denisturovsky.tm.api.service.ILoggerService;

import java.io.IOException;
import java.util.logging.*;

public final class LoggerService implements ILoggerService {

    @NotNull
    private final static String COMMANDS = "COMMANDS";

    @NotNull
    private final static String COMMANDS_FILE = "./commands.xml";

    @NotNull
    private final static String CONFIG_FILE = "/logger.properties";

    @NotNull
    private final static String ERRORS = "ERRORS";

    @NotNull
    private final static String ERRORS_FILE = "./errors.xml";

    @NotNull
    private final static String MESSAGES = "MESSAGES";

    @NotNull
    private final static String MESSAGE_FILE = "./messages.xml";

    @NotNull
    private final Logger commands = Logger.getLogger(COMMANDS);

    @NotNull
    private final ConsoleHandler consoleHandler = getConsoleHandler();

    @NotNull
    private final Logger errors = Logger.getLogger(ERRORS);

    @NotNull
    private final LogManager manager = LogManager.getLogManager();

    @NotNull
    private final Logger messages = Logger.getLogger(MESSAGES);

    @NotNull
    private final Logger root = Logger.getLogger("");

    {
        init();
        registry(commands, COMMANDS_FILE, false);
        registry(errors, ERRORS_FILE, true);
        registry(messages, MESSAGE_FILE, true);
    }

    @Override
    public void command(@Nullable final String message) {
        if (message == null || message.isEmpty()) return;
        commands.info(message);
    }

    @Override
    public void debug(@Nullable final String message) {
        if (message == null || message.isEmpty()) return;
        messages.fine(message);
    }

    @Override
    public void error(@Nullable final Exception e) {
        if (e == null) return;
        errors.log(Level.SEVERE, e.getMessage(), e);
    }

    private ConsoleHandler getConsoleHandler() {
        @NotNull final ConsoleHandler handler = new ConsoleHandler();
        handler.setFormatter(new Formatter() {

            public String format(LogRecord record) {
                return record.getMessage() + "\n";
            }
        });
        return handler;
    }

    @Override
    public void info(@Nullable final String message) {
        if (message == null || message.isEmpty()) return;
        messages.info(message);
    }

    private void init() {
        try {
            manager.readConfiguration(LoggerService.class.getResourceAsStream(CONFIG_FILE));
        } catch (@NotNull final IOException e) {
            root.severe(e.getMessage());
        }
    }

    private void registry(
            @NotNull final Logger logger,
            @NotNull final String fileName,
            final boolean isConsole
    ) {
        try {
            if (isConsole) logger.addHandler(consoleHandler);
            logger.setUseParentHandlers(false);
            logger.addHandler(new FileHandler(fileName));
        } catch (@NotNull final IOException e) {
            root.severe(e.getMessage());
        }
    }

}
