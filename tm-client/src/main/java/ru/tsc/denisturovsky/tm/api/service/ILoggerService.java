package ru.tsc.denisturovsky.tm.api.service;

import org.jetbrains.annotations.Nullable;

public interface ILoggerService {

    void command(@Nullable String message);

    void debug(@Nullable String message);

    void error(@Nullable Exception e);

    void info(@Nullable String message);

}
