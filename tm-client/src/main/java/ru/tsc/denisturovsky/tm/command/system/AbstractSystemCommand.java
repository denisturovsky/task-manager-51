package ru.tsc.denisturovsky.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.denisturovsky.tm.api.endpoint.ISystemEndpoint;
import ru.tsc.denisturovsky.tm.api.service.ICommandService;
import ru.tsc.denisturovsky.tm.api.service.IPropertyService;
import ru.tsc.denisturovsky.tm.command.AbstractCommand;
import ru.tsc.denisturovsky.tm.enumerated.Role;

public abstract class AbstractSystemCommand extends AbstractCommand {

    @NotNull
    protected ICommandService getCommandService() {
        assert serviceLocator != null;
        return serviceLocator.getCommandService();
    }

    @NotNull
    protected IPropertyService getPropertyService() {
        assert serviceLocator != null;
        return serviceLocator.getPropertyService();
    }

    @Nullable
    public Role[] getRoles() {
        return null;
    }

    @NotNull
    protected ISystemEndpoint getSystemEndpoint() {
        assert serviceLocator != null;
        return serviceLocator.getSystemEndpointClient();
    }

}
