package ru.tsc.denisturovsky.tm;

import lombok.SneakyThrows;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.denisturovsky.tm.api.service.IPropertyService;
import ru.tsc.denisturovsky.tm.listener.EntityListener;
import ru.tsc.denisturovsky.tm.service.LoggerService;
import ru.tsc.denisturovsky.tm.service.PropertyService;

import javax.jms.*;

public final class Application {

    @NotNull
    private static final String QUEUE = "LOGGER";

    @NotNull
    private static final IPropertyService propertyService = new PropertyService();

    @NotNull
    private static final String URL = "tcp://" + propertyService.getJMSServerHost() + ":" + propertyService.getJMSServerPort();

    @SneakyThrows
    public static void main(@Nullable String[] args) {
        @NotNull final LoggerService loggerService = new LoggerService();
        @NotNull final EntityListener entityListener = new EntityListener(loggerService);
        @NotNull final ConnectionFactory connectionFactory = new ActiveMQConnectionFactory(URL);
        @NotNull final Connection connection = connectionFactory.createConnection();
        connection.start();
        @NotNull final Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        @NotNull final Queue destination = session.createQueue(QUEUE);
        @NotNull final MessageConsumer messageConsumer = session.createConsumer(destination);
        messageConsumer.setMessageListener(entityListener);
    }

}
