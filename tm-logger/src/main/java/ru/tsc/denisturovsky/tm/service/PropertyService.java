package ru.tsc.denisturovsky.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.tsc.denisturovsky.tm.api.service.IPropertyService;

import java.util.Properties;

public class PropertyService implements IPropertyService {

    @NotNull
    public static final String FILE_NAME = "application.properties";

    @NotNull
    private static final String EMPTY_VALUE = "---";

    @NotNull
    private static final String JMS_HOST_DEFAULT = "localhost";

    @NotNull
    private static final String JMS_PORT_DEFAULT = "61616";

    @NotNull
    private static final String MONGODB_HOST_DEFAULT = "localhost";

    @NotNull
    private static final String MONGODB_PORT_DEFAULT = "27017";

    @NotNull
    private static final String JMS_HOST = "jms.host";

    @NotNull
    private static final String JMS_PORT = "jms.port";

    @NotNull
    private static final String MONGODB_HOST = "mongodb.host";

    @NotNull
    private static final String MONGODB_PORT = "mongodb.port";

    @NotNull
    private final Properties properties = new Properties();

    @SneakyThrows
    public PropertyService() {
        properties.load(ClassLoader.getSystemResourceAsStream(FILE_NAME));
    }

    @NotNull
    private String getEnvKey(@NotNull final String key) {
        return key.replace(".", "_").toUpperCase();
    }

    @NotNull
    private Integer getIntegerValue(
            @NotNull final String key,
            @NotNull final String defaultValue
    ) {
        return Integer.parseInt(getStringValue(key, defaultValue));
    }

    @NotNull
    @Override
    public String getJMSServerHost() {
        return getStringValue(JMS_HOST, JMS_HOST_DEFAULT);
    }

    @NotNull
    @Override
    public String getJMSServerPort() {
        return getStringValue(JMS_PORT, JMS_PORT_DEFAULT);
    }

    @NotNull
    @Override
    public String getMDBServerHost() {
        return getStringValue(MONGODB_HOST, MONGODB_HOST_DEFAULT);
    }

    @NotNull
    @Override
    public String getMDBServerPort() {
        return getStringValue(MONGODB_PORT, MONGODB_PORT_DEFAULT);
    }


    @NotNull
    private String getStringValue(@NotNull final String key) {
        return getStringValue(key, EMPTY_VALUE);
    }

    @NotNull
    private String getStringValue(
            @NotNull final String key,
            @NotNull final String defaultValue
    ) {
        if (System.getProperties().containsKey(key)) return System.getProperties().getProperty(key);
        @NotNull final String envKey = getEnvKey(key);
        if (System.getProperties().containsKey(envKey)) return System.getenv(envKey);
        return properties.getProperty(key, defaultValue);
    }

}